/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-20 16:04:20
 * @LastEditTime: 2019-09-20 16:08:07
 * @LastEditors: Please set LastEditors
 */
import App from './index'
import Vue from 'vue'

const app = new Vue(App)
app.$mount()